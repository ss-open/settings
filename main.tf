module "gitlab" {
  source            = "./modules/gitlab"
  gitlab_server_url = var.gitlab_server_url
  root_group_name   = "SS Open"
  root_group_path   = "ss-open"
  # Colors are based on CSS labels: https://www.rapidtables.com/web/css/css-color.html
  labels = {
    type__bug = {
      name        = "type::bug"
      color       = "#B22222"
      description = "Bug report to be resolved with a \"fix\" commit type."
    },
    type__feat = {
      name        = "type::feat"
      color       = "#2E8B57"
      description = "Feature requests."
    },
    type__build = {
      name        = "type::build"
      color       = "#2F4F4F"
      description = "Issues concerning the project build context like dependencies update or compilation options."
    },
    type__chore = {
      name        = "type::chore"
      color       = "#708090"
      description = "Miscellaneous changes."
    },
    type__ci = {
      name        = "type::ci"
      color       = "#FAEBD7"
      description = "Continuous Integration and Deployment related changes."
    },
    type__docs = {
      name        = "type::docs"
      color       = "#4169E1"
      description = "RTFM"
    },
    type__refactor = {
      name        = "type::refactor"
      color       = "#A0522D"
      description = "Code structure optimizations without production impact."
    },
    type__spike = {
      name        = "type::spike"
      color       = "#9932CC"
      description = "Research planification. https://en.wikipedia.org/wiki/Spike_(software_development)"
    },
    type__style = {
      name        = "type::style"
      color       = "#DB7093"
      description = "Coding style changes."
    },
    type__test = {
      name        = "type::test"
      color       = "#FF8C00"
      description = "Automated testing related changes."
    }
  }
}

module "gitlab_groups" {
  source    = "./modules/gitlab_groups"
  parent_id = module.gitlab.root_group_id
  groups = {
    ci = {
      name        = "CI"
      description = "GitLab CI related recipes and tools."
    }
    # Note: Projects of the POC group SHOULD NOT be managed by Terraform to keep flexibility.
    poc = {
      name        = "POC"
      description = "Proof Of Concept projects. Only testing here, no maintenance guarantee. A project MUST be moved out of this group if we want to make it stable."
    }
    stream = {
      name        = "Stream"
      description = "Streaming platforms related projects."
    }
    templates = {
      name        = "Templates"
      description = "Ready to use projects backed by the SS open group ecosystem."
    }
    websites = {
      name        = "Websites"
      description = "Static websites"
    }
  }
}

module "gitlab_projects" {
  source    = "./modules/gitlab_projects"
  parent_id = module.gitlab.root_group_id
  projects = {
    advent_of_code = {
      name        = "Advent of Code"
      description = "https://adventofcode.com"
    }
    editorconfig = {
      name        = "EditorConfig"
      description = "EditorConfig Configuration sample used by the projects of the ss-open group."
      topics = [
        "editorconfig",
      ]
    }
    scheduling = {
      name        = "Scheduling"
      description = "Scheduled GitLab CI configurations for the SS open group."
      topics = [
        "gitlab",
      ]
      merge_requests_access_level = "private"
    }
    settings = {
      name        = "Settings"
      description = "Global configurations of the SS open group, backed by Terraform."
      topics = [
        "terraform",
        "gitlab",
      ]
      merge_requests_access_level = "private"
    }
  }
}

module "gitlab_projects_ci" {
  source    = "./modules/gitlab_projects"
  parent_id = module.gitlab_groups.groups["ci"].id
  projects = {
    images = {
      name        = "Images"
      description = "Docker images needed by the CI related projects."
    }
    recipes = {
      name        = "Recipes"
      description = "GitLab CI recipes for advanced linting, build to deploy project workflows and automatic releases management."
    }
  }
}

module "gitlab_projects_poc" {
  source    = "./modules/gitlab_projects"
  parent_id = module.gitlab_groups.groups["poc"].id
  projects = {
    ci_testing = {
      name = "CI testing"
    }
    csv_to_leaderboard = {
      name = "CSV to leaderboard"
    }
    fabric_js = {}
    gitlab = {
      name        = "GitLab micro infrastructure"
      description = "For testing purpose"
    }
    mahitotsu_python_keystroke = {}
    react_cleave_js = {
      name = "React Cleave.js"
    }
    react_router = {
      name = "React Router"
    }
    react_tanstack = {
      name = "React TanStack"
    }
    react_wouter = {
      name = "React Wouter"
    }
    swagger_codegen = {}
    symfony         = {}
  }
}

module "gitlab_projects_stream" {
  source    = "./modules/gitlab_projects"
  parent_id = module.gitlab_groups.groups["stream"].id
  projects = {
    alerts = {
      name        = "Alerts"
      description = "Public website hosting alert sounds and videos with activation script."
    }
  }
}

module "gitlab_projects_templates" {
  source    = "./modules/gitlab_projects"
  parent_id = module.gitlab_groups.groups["templates"].id
  projects = {
    blank = {
      name        = "Blank"
      description = "A blank project with SS open group ecosystem setup."
    }
  }
}

module "gitlab_projects_websites" {
  source    = "./modules/gitlab_projects"
  parent_id = module.gitlab_groups.groups["websites"].id
  projects = {
    personal = {
      name        = "Personal"
      description = "Personal website of Soullivaneuh"
    }
  }
}
