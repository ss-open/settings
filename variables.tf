variable "gitlab_server_url" {
  type     = string
  nullable = false
  default  = "https://gitlab.com"
  validation {
    condition     = length(var.gitlab_server_url) > 0
    error_message = "Must not be empty."
  }
}

# provider: gitlab
variable "gitlab_token" {
  type = string
}
