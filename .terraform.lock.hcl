# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "16.11.0"
  constraints = "16.11.0"
  hashes = [
    "h1:BLvaIvPZHqBMaknvC2XxcnD/lD0C6RD0O0+DKGeiHAc=",
    "h1:GRNuOoJiNSTK+xp2l74M09QAHJsA+84HpLlkE0ICxBk=",
    "h1:Hw1BJh3S2wedHr5geP6YBzyfeFwvI/KsEsAr7Box3F0=",
    "h1:K3vWI+z6bU5rZRAngLYe/LmcbrlNEP+Dxlw/P97O7hk=",
    "h1:V6Os3xuK9wcSD3rijWSMUAZyhfA4ADiyWz6aUJCfhKo=",
    "h1:VzfsLdtfv4a2kp+vgaD8xa/+W/kq3vvOy0BWWIKgNn4=",
    "h1:dyxmqEcBEzd5VS853KsG/oZo700KSIXDV7XSL+p0vGc=",
    "h1:giysImSyXkhm4HbPMSwACFPsCWZOnBh/hlH3t69om/o=",
    "h1:katQhOUKHz8K8+se5amEHR7fU4eqJnInLlIPLWbKG6g=",
    "h1:qjZDmD3kXLhg/v5obRIK0fQMGQJK19YNhXK4cU7ugW0=",
    "h1:rD5swbq5jM9976/KldJT6zI9QLJ95oz5K+qEQEjPy4Y=",
    "h1:vBOkwLZ/hMTLVWAIEWtCK+hOHsz6nK0Zszyh2haZ+Qo=",
    "h1:xKVFTJ2guJoK5ouv4lgLh+0B4vFsUSNIUvBsP2DCLRg=",
    "h1:yAswlOHA3wDdojqU8OTnQlmC5dW4mD4dAJ/sMfCw/tU=",
    "zh:00e6d68c97c739f320407f76537e58218b52cea0128c01a8995820be79859810",
    "zh:14578344dc44043f537c248e04da766b79587f47327e0dfe7dac4e9e272b7c49",
    "zh:26817d48b62907fe1cc16cface8d6035395c9370dfc39e2fbb1ee7a112c10584",
    "zh:28ad3bdedd76cb7e2290a0b1a2288c5042913d46879644a6776a0fe3e109db12",
    "zh:3882e3d81e751074bf0ae2ee2008c058d6b5b797e8d3f7582c041f7657404c2d",
    "zh:402eda34a8759246f084936bdd511073abb79337ce879a5bba46c065912028f3",
    "zh:6686b2a58e973b570204c63f83f9d5bb7f641406177857fe05619c5679ffda05",
    "zh:8be6c674904b5954d51510663cc74e9d03ec7ee500f0e0e234fe85d9d7d7500c",
    "zh:a0813c74a396d14be8332dffad9f597705af1246bb9b582f149d00c86ad8e24a",
    "zh:bc782bf60c3956c61f52e0c90ab9c125e1c49f2472173ca7edf0bf99fadb05ac",
    "zh:cd9b0e82d2f14e69347c9bb2ecc8fec67238b565bd0f5f5bde4020b98af09e93",
    "zh:ef7582fee9d96d52be020512180801c18e5d4589b4e31588c235c191acbd9ddc",
    "zh:f6f4c313cc90994b122bf0af206d225125d0d7818972b74949944038d8602941",
    "zh:f712c56d17de482a6b4a43ce81d25937a8c9e882e1aa6161f413ab250ae84e65",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
  ]
}
