terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

resource "gitlab_group" "root" {
  name                   = var.root_group_name
  path                   = var.root_group_path
  wiki_access_level      = "disabled"
  project_creation_level = "maintainer"
}

resource "gitlab_group_label" "label" {
  for_each    = var.labels
  group       = gitlab_group.root.path
  name        = each.value.name
  color       = each.value.color
  description = each.value.description
}

resource "gitlab_group_badge" "gitlab_pipeline" {
  group     = gitlab_group.root.id
  link_url  = "${var.gitlab_server_url}/%%{project_path}/-/pipelines?ref=%%{default_branch}"
  image_url = "${var.gitlab_server_url}/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
}

resource "gitlab_group_badge" "gitlab_coverage" {
  group     = gitlab_group.root.id
  link_url  = "${var.gitlab_server_url}/%%{project_path}/-/jobs"
  image_url = "${var.gitlab_server_url}/%%{project_path}/badges/%%{default_branch}/coverage.svg"
}

resource "gitlab_group_badge" "gitlab_release" {
  group     = gitlab_group.root.id
  link_url  = "${var.gitlab_server_url}/%%{project_path}/-/releases"
  image_url = "${var.gitlab_server_url}/%%{project_path}/-/badges/release.svg"
}
