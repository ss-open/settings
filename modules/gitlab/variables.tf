variable "gitlab_server_url" {
  type     = string
  nullable = false
}

variable "root_group_name" {
  description = "Name of the root GitLab group to configure"
  type        = string
}

variable "root_group_path" {
  type = string
}

variable "labels" {
  description = "Group labels"
  type = map(object({
    name        = string
    color       = string
    description = string
  }))
}
