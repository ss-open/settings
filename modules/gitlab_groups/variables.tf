variable "parent_id" {
  description = "The attached parent id"
  type        = number
}

variable "groups" {
  description = "Groups list"
  type = map(object({
    name        = string
    description = string
  }))
}
