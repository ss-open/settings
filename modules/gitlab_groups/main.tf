terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

resource "gitlab_group" "group" {
  for_each         = var.groups
  parent_id        = var.parent_id
  path             = replace(each.key, "_", "-")
  name             = each.value.name
  description      = each.value.description
  visibility_level = "public"
}
