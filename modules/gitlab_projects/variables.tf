variable "parent_id" {
  description = "The attached parent id"
  type        = number
}

variable "projects" {
  description = "Group projects"
  type = map(object({
    name                        = optional(string)
    description                 = optional(string)
    topics                      = optional(list(string))
    merge_requests_access_level = optional(string, "enabled")
  }))
}
