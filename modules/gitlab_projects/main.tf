terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

resource "gitlab_project" "project" {
  for_each                                         = var.projects
  namespace_id                                     = var.parent_id
  path                                             = replace(each.key, "_", "-")
  name                                             = each.value.name != null ? each.value.name : replace(each.key, "_", "-")
  description                                      = each.value.description
  topics                                           = each.value.topics
  visibility_level                                 = "public"
  default_branch                                   = "main"
  lfs_enabled                                      = false
  wiki_enabled                                     = false
  merge_requests_enabled                           = true
  merge_requests_access_level                      = each.value.merge_requests_access_level
  only_allow_merge_if_pipeline_succeeds            = true
  allow_merge_on_skipped_pipeline                  = false
  only_allow_merge_if_all_discussions_are_resolved = true
  push_rules {
    commit_committer_check  = true
    deny_delete_tag         = true
    member_check            = true
    prevent_secrets         = true
    reject_unsigned_commits = true
  }
}

module "pipeline_schedules" {
  source   = "./pipeline_schedules"
  for_each = gitlab_project.project
  project = {
    id             = each.value.id
    default_branch = each.value.default_branch
  }
  schedules = {
    "6hourly" = "0 2,8,14,20 * * *"
    bidaily   = "0 0,12 * * *"
    daily     = "0 3 * * *"
    weekly    = "0 3 * * 0"
    monthly   = "0 3 1 * *"
  }
}
