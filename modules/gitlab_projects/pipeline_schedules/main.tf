terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

resource "gitlab_pipeline_schedule" "schedule" {
  for_each      = var.schedules
  description   = each.key
  cron          = each.value
  cron_timezone = "Europe/Paris"
  project       = var.project.id
  ref           = "refs/heads/${var.project.default_branch}"
}

resource "gitlab_pipeline_schedule_variable" "frequency" {
  for_each             = gitlab_pipeline_schedule.schedule
  project              = each.value.project
  pipeline_schedule_id = each.value.pipeline_schedule_id
  key                  = "CI_SCHEDULE_FREQUENCY"
  value                = each.value.description
}
