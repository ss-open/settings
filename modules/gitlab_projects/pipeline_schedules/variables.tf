variable "project" {
  type = object({
    id             = number
    default_branch = string
  })
}

variable "schedules" {
  type = map(string)
}
