# Settings

Global configurations of the SS open group, backed by Terraform.

## Table of contents

- [Settings](#settings)
  - [Table of contents](#table-of-contents)
  - [Local usage](#local-usage)
  - [Documentation](#documentation)
    - [References](#references)

## Local usage

To use this project locally, you need to initialize it without backend:

```sh
terraform init --backend=false
```

You MAY also need to remove the `.terraform` directory before if the state was already initialized.

## Documentation

### References

- [Official best practices](https://developer.hashicorp.com/terraform/cloud-docs/recommended-practices)
- [Google best practices](https://cloud.google.com/docs/terraform/best-practices-for-terraform)
